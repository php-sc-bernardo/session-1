<?php 

function getLetterGrade($grade){
	if($grade >= 98 && $grade <= 100){
		echo "$grade is equivalent to A+";
	}
	else if($grade >= 95 && $grade <= 97){
		echo "$grade is equivalent to A";
	}
	else if($grade >= 92 && $grade <= 94){
		echo "$grade is equivalent to A-";
	}
	else if($grade >= 89 && $grade <= 91){
		echo "$grade is equivalent to B+";
	}
	else if($grade >= 86 && $grade <= 88){
		echo "$grade is equivalent to B";
	}
	else if($grade >= 83 && $grade <= 85){
		echo "$grade is equivalent to B-";
	}
	else if($grade >= 80 && $grade <= 82){
		echo "$grade is equivalent to C";
	}
	else if($grade >= 77 && $grade <= 79){
		echo "$grade is equivalent to C+";
	}
	else if($grade >= 75 && $grade <= 76){
		echo "$grade is equivalent to C-";
	}
	else{
		echo "$grade is equivalent to D";
	}
}